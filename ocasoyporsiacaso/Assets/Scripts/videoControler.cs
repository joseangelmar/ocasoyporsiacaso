using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class videoControler : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    public Slider slider;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = (float)videoPlayer.frame / (float)videoPlayer.frameCount;
        if (slider.value>=0.99f||(Input.GetKey(KeyCode.Escape)))
        {
            Debug.Log("4");
            SceneManager.LoadScene("MenuPrincipal");
        }
    }
}
