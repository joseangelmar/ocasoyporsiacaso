using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HarbolAvilidades : MonoBehaviour
{
    public static int nMejoras;
    public Sprite Estado1;
    public Sprite Estado2;
    public Sprite Estado3;
    public Sprite Estado4;
    public Button botton;
    // Start is called before the first frame update
    void Start()
    {
        //nMejoras = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (nMejoras == 1)
        {
            botton.image.sprite = Estado1;
        }
        if (nMejoras == 2)
        {
            botton.image.sprite = Estado2;
        }
        if (nMejoras == 3)
        {
            botton.image.sprite = Estado3;
        }
        if (nMejoras == 4)
        {
            botton.image.sprite = Estado4;
        }
    }
    public void sumarVida()
    {
        if (GameManager.maxVida < 5)
        {
            if (GameManager.experiencia >= 10)
            {
                nMejoras++;
                GameManager.experiencia -= 10;
                GameManager.maxVida++;
            }

        }
    }
}
