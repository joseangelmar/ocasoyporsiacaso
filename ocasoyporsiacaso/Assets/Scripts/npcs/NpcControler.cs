using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NpcControler : MonoBehaviour
{
    #region variables
    public bool fraseterminada;
    public GameObject colider;
    public GameManager gamemanager;
    public SpriteRenderer spriteRenderer;
    public bool midas;
    public bool elvis;
    public bool snorlax;
    public bool espejo;
    public bool teresa;
    public bool anto�eta;
    //public List<Sprite> aspecto;
    private List<string> frasesMidas;
    private List<string> frasesElvis;
    private List<string> frasesSnorlax;
    private List<string> frasesEspejo;
    private List<string> frasesTeresa;
    private List<string> frasesAnto�eta;
    public Text mensaje;
    public GameObject cuadroDeDialogo;
    private string frase;
    private int fraseActual;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        fraseterminada = true;
        if (!snorlax)
        {
            colider = null;
        }
        fraseActual = 0;
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (cuadroDeDialogo != null)
        {
            cuadroDeDialogo.SetActive(false);
        }
        #region cambiador de sprite(desactivado)
        //if (midas == true)
        //{
        //    spriteRenderer.sprite = aspecto[0];
        //}
        //if (elvis == true)
        //{
        //    spriteRenderer.sprite = aspecto[1];
        //}
        //if (snorlax == true)
        //{
        //    spriteRenderer.sprite = aspecto[2];
        //}
        //if (espejo == true)
        //{
        //    spriteRenderer.sprite = aspecto[3];
        //}
        //if (teresa == true)
        //{
        //    spriteRenderer.sprite = aspecto[4];
        //}
        //if (anto�eta == true)
        //{
        //    spriteRenderer.sprite = aspecto[5];
        //}
        #endregion
        #region frases Midas
        frasesMidas = new List<string>();
        frasesMidas.Add("Rey Midas: �ATR�S O EN ORO TE CONVERTIR�S!");
        frasesMidas.Add("Nietzsche: Este lugar est� lleno de locos�");
        frasesMidas.Add("Rey Midas:COMO OSAS AL REY MIDAS LOCO LLAMAR,Utilizar� tus entra�as para hacerme un collar.");
        frasesMidas.Add("Nietzsche:Pero t� qu� haces aqu�?");
        frasesMidas.Add("Rey Midas:Un d�a me echaron una maldici�n, por culpa de la avaricia acab� en esta situaci�n..");
        frasesMidas.Add("Rey Midas: adem�s te dir� que para volver a empezar un gran pecado cometer�s, un suicidio y volver�s al inicio");
        frasesMidas.Add("Rey Midas: Tra�me unos guantes para mi cuerpo tocar y aldo de oro te podr� dar ");
        //Cuando le llevas los guantes (Los da Tere) te da dinero
        #endregion
        #region frases Elvis
        frasesElvis = new List<string>();
        frasesElvis.Add("Elvis: You ain't nothing but a hound dog�");
        frasesElvis.Add("Crying all the time�Oh Yeah");
        frasesElvis.Add("Nietzsche: Elvis? Pero t� qu� haces aqu�?");
        frasesElvis.Add("Elvis: Demasiado Rock n Roll. Oh Yeah!");
        frasesElvis.Add("Nietzsche: Te ves algo cambiado�");
        frasesElvis.Add("Elvis: Es todo agua, tu te ves en los huesos amigo, por casualidad no te sobrar� alguno que echarme al buche. Oh Yeah");
        frasesElvis.Add("Elvis: Oh Yeah, tra�me un poco de comida y acambio te dir�...");
        frasesElvis.Add("Elvis: Necesitar�s una flauta de pan para que en la tienda te dejen entrar, una llave para por su puerta pasar");
        //Cuando le llevas la comida no te da nada
        #endregion
        #region frases Snorlax
        frasesSnorlax = new List<string>();
        frasesSnorlax.Add("Una criatura salvaje bloquea el camino, quiz�s un objeto m�gico podr�a despertarle�");
        #endregion
        #region frases Espejo
        frasesEspejo = new List<string>();
        frasesEspejo.Add("Nietzsche: Espejito espejito� qui�n es el m�s guapo de este reino");
        frasesEspejo.Add("Espejo: YO, SOY YO");
        frasesEspejo.Add("Nietzsche: Y t� eres�?");
        frasesEspejo.Add("Espejo: Soy quien quiera ser.");
        frasesEspejo.Add("Nietzsche: Es decir, nadie.");
        frasesEspejo.Add("Espejo: �Y t� eres?");
        frasesEspejo.Add("Nietzsche: Me llamo  Friedrich, y busco el cielo");
        frasesEspejo.Add("Espejo: Pues me temo que no lo vas a poder ver.");
        frasesEspejo.Add("Nietzsche: Y eso?");
        frasesEspejo.Add("Yo tengo las llaves del cielo y no se las pienso entregar a nadie a menos que me traigan lo que m�s envidio.");
        //Cuando le das la cabeza de mariaAnto�eta te da las llaves para pasar al infierno

        #endregion
        #region frases Teresa
        frasesTeresa = new List<string>();
        frasesTeresa.Add("Tere: Wats up bby?");
        frasesTeresa.Add("Nietzsche: Teresa?");
        frasesTeresa.Add("Tere: Claro que s� Rufi�n");
        frasesTeresa.Add("Nietzsche: �Pero t� qu� haces aqu�?");
        frasesTeresa.Add("Tere: Me aceptaron en el cielo pero al parecer no est� bien visto ji�arse a m�s de tres arc�ngeles a la vez�");
        frasesTeresa.Add("Nietzsche: Creo que mejor sigo mi camino�");
        frasesTeresa.Add("Espera! Podr�as buscarme mi Diu? En el cielo me los daban gratis pero aqu� todo el mundo va a pelo. A cambio te dar� unos guantes m�gicos que utilizo como mi cond�n, todo una ganga");
        //Si le das el diu que esta en gula te da los guantes 

        #endregion
        #region frases Anto�eta
        frasesAnto�eta = new List<string>();
        frasesAnto�eta.Add("Anto�eta: hummm� hummm�");
        frasesAnto�eta.Add("Nietzsche: Perdona? Para llegar al castillo de  Sat�n?");
        frasesAnto�eta.Add("Maria Anto�eta: hummm� hummm�");
        frasesAnto�eta.Add("Nietzsche: Ah si? PUES TE ROBO LA CABEZA.");



        #endregion
    }

    // Update is called once per frame
    void Update()
    {
        if (snorlax)
        {
            if (GameManager.flautaConseguida==true)
            {
                colider.SetActive(false);
            }
            else
            {
                colider.SetActive(true);
            }
        }

        #region Cambiador de frases
        if (midas == true)
        {

            if (fraseActual >= frasesMidas.Count)
            {
                //Debug.Log("4");
                fraseActual = 0;
            }
            frase = frasesMidas[fraseActual];
        }
        if (elvis == true)
        {
            frase = frasesElvis[fraseActual];
            if (fraseActual > frasesElvis.Count)
            {
                fraseActual = 0;
            }
        }
        if (snorlax == true)
        {
            frase = frasesSnorlax[fraseActual];
            if (fraseActual > frasesSnorlax.Count)
            {
                fraseActual = 0;
            }
        }
        if (espejo == true)
        {
            frase = frasesEspejo[fraseActual];
            if (fraseActual > frasesEspejo.Count)
            {
                fraseActual = 0;
            }
        }
        if (teresa == true)
        {
            frase = frasesTeresa[fraseActual];
            if (fraseActual > frasesTeresa.Count)
            {
                fraseActual = 0;
            }
        }
        if (anto�eta == true)
        {
            frase = frasesAnto�eta[fraseActual];
            if (fraseActual > frasesAnto�eta.Count)
            {
                fraseActual = 0;
            }
        }
        #endregion
    }
    private void OnTriggerStay2D(Collider2D collision)
    {

        if ((mensaje != null) || (cuadroDeDialogo != null))
        {
            //Debug.Log("1"+collision.name);
            if (collision.CompareTag("Player"))
            {
                //Debug.Log("2");
                if (Input.GetKey(KeyCode.E) && (fraseterminada == true))
                {
                    Debug.Log("dialogo");
                    fraseterminada = false;
                    fraseActual++;
                    StartCoroutine(Print());
                }
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            cuadroDeDialogo.SetActive(false);

        }
    }

    IEnumerator Print()
    {
        mensaje.text = null;
        foreach (char caracter in frase)
        {
            mensaje.text = mensaje.text + caracter;
            yield return new WaitForSeconds(0.1f);
        }
        fraseterminada = true;
        Debug.Log("dialogoo");
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((mensaje != null) || (cuadroDeDialogo != null))
        {
            if (collision.CompareTag("Player"))
            {
                cuadroDeDialogo.SetActive(true);
                StartCoroutine(Print());
                fraseActual++;
                fraseterminada = false;
            }
        }
    }
}
