using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControler : MonoBehaviour
{
    public GameObject tutorial;
    private void Start()
    {
        tutorial.SetActive(false);
    }
    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape)|| Input.GetMouseButtonDown(0))
        {
            tutorial.SetActive(false);
        }
    }
    public void PlayLevel()
    {
        SceneManager.LoadScene("Level1");
    }
    public void QuitGame()
    {
        // Sale del juego
        Application.Quit();
        Debug.Log("Te has salido");
    }
    public void Menu()
    {
        SceneManager.LoadScene("MenuPrincipal");
    }
    public void Limbo()
    {
        SceneManager.LoadScene("Limbo");
    }
    public void Tutorial()
    {
        tutorial.SetActive(true);
    }
}
