using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiendaControler : MonoBehaviour
{
    public GameObject tienda;
    public GameObject bocadillo;
    public int inventarioVida;
    public PlayerControler playerCont;
    // Start is called before the first frame update
    void Start()
    {
        bocadillo.SetActive(false);
        inventarioVida = 3;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player")&&(Input.GetKeyDown(KeyCode.E)))
        {
            tienda.SetActive(true);
        }     
    }
    public void exit()
    {
        tienda.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            bocadillo.SetActive(true);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            bocadillo.SetActive(false);
        }
    }
    public void vialVida()
    {
        if (GameManager.coins >= 5)
        {
            GameManager.coins -= 5;
            Debug.Log("curado");
            GameManager.NVidas++;
        }
        
        //inventarioVida;
    }
    public void botas()
    {
        if (GameManager.coins >= 5)
        {
            GameManager.coins -=5;
            Debug.Log("botas");
            playerCont.botas = true;
        }
       
        //playerCont.maxSpeed=25;
    }
    public void capa()
    {
        if (GameManager.coins >= 5)
        {
            GameManager.coins -= 5;
            Debug.Log("capa");
            playerCont.capa = true;
        }
        
        //Debug.Log("capas");
    }
    public void fuego()
    {
        if (GameManager.coins >= 5)
        {
            GameManager.coins -= 5;
            playerCont.fuego = true;
            Debug.Log("fuego");
        }
        
    }
}
