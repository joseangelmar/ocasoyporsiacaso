using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boomerangcontroler : MonoBehaviour
{
    public GameObject player;
    public PlayerControler playercont;
    public int boomerangSpeed;
    public bool lanzado;
    //public GameObject startPoint;
    public GameObject endPoint;
    private bool buelta;
    public GameObject set;
    void Start()
    {
        playercont.slot = false;
        buelta = false;
        transform.position = player.transform.position+new Vector3(0,2.4f,0);
        Vector3 numero = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        numero.z = 0;
        endPoint.transform.position= numero ;
    }
    // Update is called once per frame
    void Update()
    {
        if (buelta == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, endPoint.transform.position, boomerangSpeed * Time.deltaTime);
        }
        if (transform.position == endPoint.transform.position)
        {
            buelta = true;
        }
        if (buelta == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position+new Vector3 (0,0.5f,0), boomerangSpeed * Time.deltaTime);
        }
        if (transform.position == player.transform.position+ new Vector3(0, 0.5f, 0) && (buelta==true))
        {
            playercont.slot = true;
            Destroy(set);
        }
    }
}
