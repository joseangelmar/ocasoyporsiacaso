using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Coleccionable1 : MonoBehaviour
{
    public bool flauta;
    public bool hamburguesa;
    public bool diu;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.flautaConseguida)
        {
            if (flauta)
            {
                Destroy(gameObject);
            }
        }
        if (GameManager.hamburguesaConseguida)
        {
            if (hamburguesa)
            {
                Destroy(gameObject);
            }
        }
        if (GameManager.diuConseguido)
        {
            if (diu)
            {
                Destroy(gameObject);
            }
        }
    }
}
