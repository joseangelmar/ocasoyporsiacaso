using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class EnemyControler : MonoBehaviour
{
    private bool mirandod;
    private SpriteRenderer sprite;
    public bool mele;
    public bool rat;
    public bool range;
    public bool tank;
    public GameObject startPoint;
    public GameObject endPoint;
    public float enemySpeed;
    private bool directionD;
    public Slider vidaSlider;
    public GameObject set;
    public int cantidadm;
    public int cantidadx;
    public GameObject monedas;
    public GameObject exp;
    private bool monedasexpul;
    private bool expexpul;
    private bool expulsados;
    private bool settarget;
    public GameObject target;
    private float xyo;
    private float xtarget;
    public int piso;
    //public PlayerControler contplay;
    private bool targetset;
    public GameObject piss;
    public GameObject Brazo;
    public SpriteRenderer breazorenderer;
    public Animator brazoAnimator;
    private void Start()
    {
        if (!mele)
        {
            Brazo = null;
            breazorenderer = null;
            brazoAnimator = null;
        }
        targetset = false;
        settarget = false;
        //cantidadm = 1;
        //cantidadx = 1;
        monedasexpul = false;
        expexpul = false;
        expulsados = false;
        if (tank == true)
        {
            enemySpeed = 1;
            cantidadm = Random.Range(1, 5);
            cantidadx = Random.Range(1, 5);
        }
        if (mele == true)
        {
            enemySpeed = 2;
            cantidadm = Random.Range(1, 3);
            cantidadx = Random.Range(1, 3);
        }
        if (range == true)
        {
            enemySpeed = 2;
            cantidadm = Random.Range(1, 2);
            cantidadx = Random.Range(1, 2);
        }
        vidaSlider.value = 1;
        //if (directionD)
        //{
        //    transform.position = startPoint.transform.position;
        //}
        //else
        //{
        //    transform.position = endPoint.transform.position;
        //}
    }
    private void Update()
    {
        #region Ataque mele
        if (mele == true)
        {
            if (Mathf.Abs(transform.position.x - target.transform.position.x) < 3)
            {
                brazoAnimator.SetTrigger("atacar");
                if (mirandod)
                {
                    RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(.5f, 1, 0), Vector2.right, 3);
                    Debug.DrawRay(transform.position + new Vector3(1, 2, 0), Vector2.right, Color.red, 1);
                    if ((hit.collider != null) && (hit.collider.CompareTag("Player")))
                    {
                        Debug.Log("tocado");
                        if (GameManager.ultimoDaņo - Time.realtimeSinceStartup < -1)
                        {
                            GameManager.NVidas--;
                            GameManager.ultimoDaņo = Time.realtimeSinceStartup;
                        }

                    }
                }
                if (!mirandod)
                {
                    RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(-1, 1, 0), Vector2.left, 3);
                    Debug.DrawRay(transform.position + new Vector3(-1, 2, 0), Vector2.left, Color.green, 1);
                    if ((hit.collider != null) && (hit.collider.CompareTag("Player")))
                    {
                        if (GameManager.ultimoDaņo - Time.realtimeSinceStartup < -1)
                        {
                            GameManager.NVidas--;
                            //GameObject.Find("AudioManager").transform.Find("vida").GetComponent<AudioSource>().Play();
                            GameManager.ultimoDaņo = Time.realtimeSinceStartup;
                        }
                        Debug.Log("tocado");
                    }
                }
            }
        }
        #endregion
        if (range == true)
        {
            if (Mathf.Abs(target.transform.position.x - transform.position.x) < 1)
            {
                Instantiate(piss, transform.position, Quaternion.identity);
            }
        }
        if (piso == PlayerControler.pisoActual)
        {
            targetset = true;
        }
        else
        {
            targetset = false;
        }
        if (targetset == true)
        {
            if (tank == true)
            {
                enemySpeed = 4;
            }
            if (range)
            {
                transform.position = Vector3.MoveTowards(transform.position, target.transform.position + new Vector3(0, 6, 0), enemySpeed * Time.deltaTime);
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, target.transform.position, enemySpeed * Time.deltaTime);
            }
            if (transform.position.x < target.transform.position.x)
            {
                GetComponent<SpriteRenderer>().flipX = false;
                if (breazorenderer != null)
                {
                    breazorenderer.flipX = false;
                    if (rat)
                    {
                        Brazo.transform.localPosition = new Vector2(-0.9200001f, 0.1800001f);
                    }
                    else
                    {
                        Brazo.transform.localPosition = new Vector2(-0.66f, 2.78f);
                    }
                }

                mirandod = true;

            }
            else
            {
                if (breazorenderer != null)
                {
                    breazorenderer.flipX = true;
                    if (rat)
                    {
                        Brazo.transform.localPosition = new Vector2(0.99f, 0.1800001f);
                    }
                    else
                    {
                        Brazo.transform.localPosition = new Vector2(0.73f, 2.78f);
                    }
                }
                GetComponent<SpriteRenderer>().flipX = true;
                mirandod = false;
            }
        }
        else
        {
            if (!directionD)
            {
                transform.position = Vector3.MoveTowards(transform.position, endPoint.transform.position, enemySpeed * Time.deltaTime);
                if (Mathf.Abs(transform.position.x - endPoint.transform.position.x) < .1f)
                {
                    directionD = true;
                    GetComponent<SpriteRenderer>().flipX = false;
                    if (breazorenderer != null)
                    {
                        breazorenderer.flipX = false;
                        if (rat)
                        {
                            Brazo.transform.localPosition = new Vector2(-0.9200001f, 0.1800001f);
                        }
                        else
                        {
                            Brazo.transform.localPosition = new Vector2(-0.66f, 2.78f);
                        }
                    }
                    mirandod = true;
                }
            }
            if (directionD)
            {
                transform.position = Vector3.MoveTowards(transform.position, startPoint.transform.position, enemySpeed * Time.deltaTime);
                if (Mathf.Abs(transform.position.x - startPoint.transform.position.x) < .1f)
                {
                    directionD = false;
                    GetComponent<SpriteRenderer>().flipX = true;
                    if (breazorenderer != null)
                    {
                        breazorenderer.flipX = true;
                        if (rat)
                        {
                            Brazo.transform.localPosition = new Vector2(0.99f, 0.1800001f);
                        }
                        else
                        {
                            Brazo.transform.localPosition = new Vector2(0.73f, 2.78f);
                        }
                    }
                    mirandod = false;
                }
            }
        }
        if (vidaSlider.value == 0 && expulsados == false)
        {
            StartCoroutine(Monedas(1));
            StartCoroutine(Experienci(1));
            expulsados = true;
        }
        if (monedasexpul == true && expexpul == true)
        {
            Destroy(set);
            GameObject.Find("AudioManager").transform.Find("muerteEnemigo").GetComponent<AudioSource>().Play();
        }

    }
    public void OnDrawGizmos()
    {
        Gizmos.DrawLine(startPoint.transform.position, endPoint.transform.position);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (tank == true)
        {
            if (collision.CompareTag("playerAtac2"))
            {
                vidaSlider.value -= PlayerControler.atact2 / 3;
            }
        }
        if (mele == true)
        {
            if (collision.CompareTag("playerAtac2"))
            {
                vidaSlider.value -= PlayerControler.atact2 / 2;
            }
        }
        if (range == true)
        {
            if (collision.CompareTag("playerAtac2"))
            {
                vidaSlider.value -= PlayerControler.atact2;
            }
        }
        if (tank == true)
        {
            if (collision.CompareTag("playerAtac1"))
            {
                vidaSlider.value -= PlayerControler.atact2 / 3;
                if (GameManager.ultimoDaņo - Time.realtimeSinceStartup < -1)
                {
                    vidaSlider.value -= 0.1f;
                    GameManager.ultimoDaņo = Time.realtimeSinceStartup;
                }
            }
        }
        if (mele == true)
        {
            if (collision.CompareTag("playerAtac1"))
            {
                vidaSlider.value -= PlayerControler.atact2 / 2;
            }
        }
        if (range == true)
        {
            if (collision.CompareTag("playerAtac1"))
            {
                vidaSlider.value -= PlayerControler.atact2;
            }
        }
    }
    //private void OnTriggerStay2D(Collider2D collision)
    //{
    //    if (collision.CompareTag("Player"))
    //    {
    //        target = collision.gameObject;
    //        settarget = true;
    //    }
    //    else
    //    {
    //        settarget = false;
    //    }
    //}
    IEnumerator Monedas(int seconds)
    {
        for (int i = 0; i < cantidadm; i++)
        {
            yield return new WaitForSeconds(0.05f);
            Instantiate(monedas, gameObject.transform.position, Quaternion.identity);
        }
        monedasexpul = true;
    }
    IEnumerator Experienci(int seconds)
    {
        for (int i = 0; i < cantidadx; i++)
        {
            yield return new WaitForSeconds(0.5f);
            Instantiate(exp, gameObject.transform.position, Quaternion.identity);
        }
        expexpul = true;
    }
}
