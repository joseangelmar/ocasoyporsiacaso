using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class palanca : MonoBehaviour
{
    public bool activado;
    private SpriteRenderer Spr;
    // Start is called before the first frame update
    void Start()
    {
        activado = false;
        Spr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (activado)
        {
            Spr.flipX = true;
        }
        else
        {
            Spr.flipX = false;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            
            if (Input.GetKeyDown(KeyCode.E))
            {
                Debug.Log("Player");
                activado = true;
            }
        }
    }
}
