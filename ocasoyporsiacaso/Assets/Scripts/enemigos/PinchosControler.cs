using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinchosControler : MonoBehaviour
{
    private Rigidbody2D rb2d;
    public bool fijo;
    public bool caida;
    public bool tiempo;
    public bool callendo;
    public bool pis;
    // Start is called before the first frame update
    void Start()
    {
        callendo = false;
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.isKinematic = true;
        rb2d.gravityScale = 0;
        if (pis)
        {
            rb2d.gravityScale = 1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (caida == true)
        {
            RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - 1f), Vector2.down, 20);
            //if ((hit.collider != null))
            //{
            //    Debug.Log(hit.collider.name);
            //}
            if ((hit.collider != null) && (hit.collider.CompareTag("Player")))
            {
                //Debug.Log("aa");
                rb2d.isKinematic = false;
                rb2d.gravityScale = 3;
                callendo = true;
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (GameManager.ultimoDaņo - Time.realtimeSinceStartup < -1)
            {
                GameManager.NVidas--;
                //GameObject.Find("AudioManager").transform.Find("vida").GetComponent<AudioSource>().Play();
                GameManager.ultimoDaņo = Time.realtimeSinceStartup;
            }
            if (caida == true)
            {
                Destroy(gameObject);
            }
            if (pis == true)
            {
                Destroy(gameObject);
            }
        }
        if (collision.CompareTag("floor"))
        {
            if (callendo == true || (pis == true))
            {
                Destroy(gameObject);
            }
        }
    }
}
