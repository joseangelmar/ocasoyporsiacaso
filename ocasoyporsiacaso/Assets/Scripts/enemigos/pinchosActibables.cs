using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pinchosActibables : MonoBehaviour
{
    public GameObject palanca;
    public palanca planc;
    public bool activados;
    public bool golpeando;
    public bool da�ando;
    private Collider2D coll;
    public GameObject pinchos;
    // Start is called before the first frame update
    void Start()
    {
        coll = GetComponent<Collider2D>();
        pinchos.SetActive(true);
        coll.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        activados = planc.activado;
        if (golpeando == true&&(da�ando==false))
        {
            da�ando = true;
            if (GameManager.ultimoDa�o - Time.realtimeSinceStartup < -1)
            {
                GameManager.NVidas--;
                //GameObject.Find("AudioManager").transform.Find("vida").GetComponent<AudioSource>().Play();
                GameManager.ultimoDa�o = Time.realtimeSinceStartup;
            }
        }
        if (planc.activado == true)
        {
            pinchos.SetActive(false);
            coll.enabled=false;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            golpeando = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            golpeando = false;
            da�ando = false;
        }
    }

}
