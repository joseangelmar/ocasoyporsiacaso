using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static bool flautaConseguida;
    public static bool hamburguesaConseguida;
    public static bool diuConseguido;
    public static bool guanteConseguido;
    public static bool llaveConseguida;
    public static bool cabezaConseguida;
    public static bool gameOver;
    public GameObject gameOverText;
    public GameObject pauseText;
    public static int experiencia;
    private string xpvalue;
    public Text xpText;
    public static int coins;
    private string coinvalue;
    public Text coinText;
    public bool executedGame;
    public GameObject glovalVolumePause;
    public static int vidamax;
    public List<GameObject> obgetos;
    public List<GameObject> vidas;
    public List<Sprite> vidaStado;
    public GameObject sangre;
    public static int maxVida=1;
    public static bool sangrando;
    public static int NVidas
    {
        get
        {
            return nVidas;
        }
        set
        {
            if (value == nVidas - 1)
            {
                daņoRecivido();
            }
            nVidas = value;
        }
    }
    public static int nVidas;
    public PlayerControler playercontroler;
    public GameObject bigote;
    public GameObject bigoteGris;
    public static float ultimoDaņo;
    // Start is called before the first frame update
    void Start()
    {
        sangrando = false;
        ultimoDaņo = 0;
        bigote.SetActive(true);
        bigoteGris.SetActive(false);
        //maxVida=nVidas;
        NVidas = maxVida;
        gameOver = false;
        coins = 0;
        executedGame = true;
        Time.timeScale = 1f;
        PlayerControler.pisoActual = 0;
        #region Vida Maxima
        if (maxVida == 1)
        {
            vidas[1].SetActive(false);
            vidas[2].SetActive(false);
            vidas[3].SetActive(false);
            vidas[4].SetActive(false);
        }
        if (maxVida == 2)
        {
            vidas[2].SetActive(false);
            vidas[3].SetActive(false);
            vidas[4].SetActive(false);
        }
        if (maxVida == 3)
        {
            vidas[3].SetActive(false);
            vidas[4].SetActive(false);
        }
        if (maxVida == 4)
        {
            vidas[4].SetActive(false);
        }
        #endregion
        
    }

    // Update is called once per frame
    void Update()
    {
        #region
        if (hamburguesaConseguida == true)
        {
            obgetos[0].SetActive(true);
        }
        if (diuConseguido == true)
        {
            obgetos[1].SetActive(true);
        }
        if (flautaConseguida == true)
        {
            obgetos[2].SetActive(true);
        }
        if (guanteConseguido == true)
        {
            obgetos[3].SetActive(true);
        }
        if (cabezaConseguida == true)
        {
            obgetos[4].SetActive(true);
        }
        if (llaveConseguida == true)
        {
            obgetos[5].SetActive(true);
        }
        #endregion
        if (sangrando == true)
        {
            StartCoroutine(Blood());
            sangrando = false;
        }
        if (playercontroler.slot == true)
        {
            bigote.SetActive(true);
            bigoteGris.SetActive(false);
        }
        else
        {
            bigote.SetActive(false);
            bigoteGris.SetActive(true);
        }
        if (gameOver == true)
        {
            glovalVolumePause.SetActive(true);
            gameOverText.SetActive(true);
            //Invoke("ResetearPartida", 5f);
            Time.timeScale = 0f;
        }
        xpvalue = experiencia.ToString();
        xpText.text = xpvalue;
        coinvalue = coins.ToString();
        coinText.text = coinvalue;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ChangeExecutedGame();
        }
        if (NVidas == 0)
        {
            vidas[0].GetComponent<Image>().sprite = vidaStado[1];
            vidas[1].GetComponent<Image>().sprite = vidaStado[1];
            vidas[2].GetComponent<Image>().sprite = vidaStado[1];
            vidas[3].GetComponent<Image>().sprite = vidaStado[1];
            vidas[4].GetComponent<Image>().sprite = vidaStado[1];
            gameOver = true;
            GameObject.Find("AudioManager").transform.Find("vida").GetComponent<AudioSource>().Play();
        }
        #region Numero de vidas
        if (NVidas == 1)
        {
            vidas[0].GetComponent<Image>().sprite = vidaStado[0];
            vidas[1].GetComponent<Image>().sprite = vidaStado[1];
            vidas[2].GetComponent<Image>().sprite = vidaStado[1];
            vidas[3].GetComponent<Image>().sprite = vidaStado[1];
            vidas[4].GetComponent<Image>().sprite = vidaStado[1];
            GameObject.Find("AudioManager").transform.Find("vida").GetComponent<AudioSource>().Play();
        }
        if (NVidas == 2)
        {
            vidas[0].GetComponent<Image>().sprite = vidaStado[0];
            vidas[1].GetComponent<Image>().sprite = vidaStado[0];
            vidas[2].GetComponent<Image>().sprite = vidaStado[1];
            vidas[3].GetComponent<Image>().sprite = vidaStado[1];
            vidas[4].GetComponent<Image>().sprite = vidaStado[1];
            GameObject.Find("AudioManager").transform.Find("vida").GetComponent<AudioSource>().Play();
        }
        if (NVidas == 3)
        {
            vidas[0].GetComponent<Image>().sprite = vidaStado[0];
            vidas[1].GetComponent<Image>().sprite = vidaStado[0];
            vidas[2].GetComponent<Image>().sprite = vidaStado[0];
            vidas[3].GetComponent<Image>().sprite = vidaStado[1];
            vidas[4].GetComponent<Image>().sprite = vidaStado[1];
            GameObject.Find("AudioManager").transform.Find("vida").GetComponent<AudioSource>().Play();
        }
        if (NVidas == 4)
        {
            vidas[0].GetComponent<Image>().sprite = vidaStado[0];
            vidas[1].GetComponent<Image>().sprite = vidaStado[0];
            vidas[2].GetComponent<Image>().sprite = vidaStado[0];
            vidas[3].GetComponent<Image>().sprite = vidaStado[0];
            vidas[4].GetComponent<Image>().sprite = vidaStado[1];
            GameObject.Find("AudioManager").transform.Find("vida").GetComponent<AudioSource>().Play();
        }
        if (NVidas == 5)
        {
            vidas[0].GetComponent<Image>().sprite = vidaStado[0];
            vidas[1].GetComponent<Image>().sprite = vidaStado[0];
            vidas[2].GetComponent<Image>().sprite = vidaStado[0];
            vidas[3].GetComponent<Image>().sprite = vidaStado[0];
            vidas[4].GetComponent<Image>().sprite = vidaStado[0];
            GameObject.Find("AudioManager").transform.Find("vida").GetComponent<AudioSource>().Play();
        }
        #endregion
    }
    public void ResetearPartida()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void ChangeExecutedGame()
    {
        executedGame = !executedGame;
        if (executedGame==true)
        {
            ejecutandose();
        }
        else
        {
            pausado();
        }
    }
    public bool IsExecutedGame()
    {
        return executedGame;
    }
    public void ejecutandose()
    {
        Time.timeScale = 1f;       
        glovalVolumePause.SetActive(false);
        pauseText.SetActive(false);
        executedGame = true;
    }
    public void pausado()
    {
        Time.timeScale = 0f;
        glovalVolumePause.SetActive(true);
        pauseText.SetActive(true);
        executedGame = false;
    }
    public static void daņoRecivido()
    {
        GameObject.Find("AudioManager").transform.Find("vida").GetComponent<AudioSource>().Play();
        sangrando = true;
    }
    public IEnumerator Blood()
    {
        sangre.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        sangre.SetActive(false);
    }
}
