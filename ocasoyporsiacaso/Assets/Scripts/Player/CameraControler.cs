using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControler : MonoBehaviour
{
    public GameObject player;
    private Vector3 posicion;
    private float y;
    private List<float> ypisos;
    // Start is called before the first frame update
    void Start()
    {
        ypisos = new List<float>();
        ypisos.Add(5.2f);
        ypisos.Add(-15f);
        ypisos.Add(-38);
        ypisos.Add(-61.9f);
        ypisos.Add(-84.5f);
        ypisos.Add(-106f);
        ypisos.Add(-129f);
        //posicion = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        y = ypisos[PlayerControler.pisoActual];
        transform.position = new Vector3(player.transform.position.x+2,y,-10);
    }
}
