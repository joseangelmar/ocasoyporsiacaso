using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class PlayerControler : MonoBehaviour
{
    //public float maxSpeed = 5;
    public float speed;
    public float jumpForce = 5;
    private SpriteRenderer mySpriteRenderer;
    private Rigidbody2D rb2d;
    private Animator anim;
    public bool isGrounded;
    // los da�os tienen que estar entre 0 y 1;
    public static float atact2 = 0.8f;
    public GameObject boomerang;
    public GameObject boomerangFuego;
    public bool slot;
    public float enfriamiento = 0.2f;
    public bool botas;
    public bool capa;
    public bool fuego;
    public int nsaltos;
    public static int pisoActual;
    public GameManager gamemanager;


    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        slot = true;
        speed = 5;
        capa = false;
        botas = false;
    }
    // Start is called before the first frame update
    void Start()
    {
        pisoActual = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (botas)
        {
            speed = 10;
        }
        #region Animacion salto
        if (isGrounded == false)
        {
            anim.SetBool("OnEir", true);
        }
        else
        {
            anim.SetBool("OnEir", false);
        }
        if (rb2d.velocity.y < 0)
        {
            anim.SetBool("bajando", true);
        }
        else { anim.SetBool("bajando", false); }
        if (rb2d.velocity.y > 0)
        {
            anim.SetBool("subiendo", true);
        }
        else { anim.SetBool("subiendo", false); }
        #endregion
        //Debug.Log(nsaltos);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 0.05f, LayerMask.GetMask("floor"));
        if ((hit.collider != null) && (hit.collider.CompareTag("floor")))
        {
            //Debug.Log("kkk");
            isGrounded = true;
            nsaltos = 0;
        }
        else
        {
            isGrounded = false;
        }
        if (!Input.anyKey)
        {
            anim.SetBool("teclapulsada", false);
        }
        anim.SetFloat("Speed", Mathf.Abs(rb2d.velocity.x));
        if ((Input.GetKey(KeyCode.D)) || (Input.GetKey(KeyCode.RightArrow)))
        {

            mySpriteRenderer.flipX = false;
            anim.SetBool("teclapulsada", true);
        }
        if ((Input.GetKey(KeyCode.A)) || (Input.GetKey(KeyCode.LeftArrow)))
        {

            mySpriteRenderer.flipX = true;

            anim.SetBool("teclapulsada", true);
        }
        float h = Input.GetAxis("Horizontal");
        if ((h == 0) && (isGrounded))
        {
        }
        else
        {
            rb2d.velocity = new Vector2(speed * h, rb2d.velocity.y);


        }
        if (capa == false)
        {
            if ((Input.GetKeyDown("space")) && (isGrounded))
            {
                salto();
            }
        }
        if (capa == true)
        {
            if ((Input.GetKeyDown("space")) && (nsaltos <= 1))
            {
                salto();
                nsaltos++;
            }
        }
        if (gamemanager.executedGame == true)
        {

            if (Input.GetMouseButtonDown(0) && slot == true)
            {
                slot = false;
                StartCoroutine(UsingYield(1));
                if (!fuego)
                {
                    Instantiate(boomerang).GetComponentInChildren<boomerangcontroler>().player = this.gameObject;
                }
                else
                {
                    Instantiate(boomerangFuego).GetComponentInChildren<boomerangcontroler>().player = this.gameObject;
                }

            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemigos"))
        {
            GameManager.NVidas--;
            GameObject.Find("AudioManager").transform.Find("vida").GetComponent<AudioSource>().Play();

        }
        if (collision.gameObject.CompareTag("lava"))
        {
            GameManager.NVidas = 0;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("flauta"))
        {
            GameManager.flautaConseguida = true;
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("hamburguesa"))
        {
            GameManager.hamburguesaConseguida = true;
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("diu"))
        {
            GameManager.diuConseguido = true;
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("tere"))
        {
            if (GameManager.diuConseguido == true)
            {
                GameManager.guanteConseguido = true;
                GameManager.coins +=5;
            }
        }
        if (collision.gameObject.CompareTag("antoneta"))
        {
            GameManager.cabezaConseguida = true;
        }
        if (collision.gameObject.CompareTag("espejo"))
        {
            if (GameManager.cabezaConseguida == true)
            {
                GameManager.llaveConseguida = true;
            }
        }
        if (collision.gameObject.CompareTag("cerradura"))
        {
            if (GameManager.llaveConseguida == true)
            {
                SceneManager.LoadScene("Creditos");
            }
        }
    }
    private void salto()
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, jumpForce);
    }
    IEnumerator UsingYield(int seconds)
    {
        yield return new WaitForSeconds(5);
        slot = true;
    }
}
