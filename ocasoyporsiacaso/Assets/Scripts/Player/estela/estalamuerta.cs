using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class estalamuerta : MonoBehaviour
{

    public GameObject estela;

    // Start is called before the first frame update
    void Start()
    {
        
        StartCoroutine(UsingYield(0.25f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator UsingYield(float seconds)
    {
        yield return new WaitForSeconds(0.10f);
        Destroy(estela);   
    }
}
