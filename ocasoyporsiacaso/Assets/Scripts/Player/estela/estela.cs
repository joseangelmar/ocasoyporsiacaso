using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class estela : MonoBehaviour
{
    public float timeBtwSpawns;
    public float startTimeBtwSpawns;
    public GameObject nichee;
    public GameObject nicheizq;
    private bool mirandoD;
    private void Start()
    {
        mirandoD = true;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.D)|| Input.GetKeyDown(KeyCode.RightArrow))
        {
            mirandoD = true;
        }
        if (Input.GetKeyDown(KeyCode.A)|| Input.GetKeyDown(KeyCode.LeftArrow))
        {
            mirandoD = false;
        }
        if (mirandoD == true)
        {
            if (timeBtwSpawns <= 0)
            {
                Instantiate(nichee, transform.position, Quaternion.identity);
                timeBtwSpawns = startTimeBtwSpawns;

            }
            else
            {
                timeBtwSpawns -= Time.deltaTime;
            }
        }
        if (mirandoD == false)
        {
            if (timeBtwSpawns <= 0)
            {
                Instantiate(nicheizq, transform.position, Quaternion.identity);
                timeBtwSpawns = startTimeBtwSpawns;

            }
            else
            {
                timeBtwSpawns -= Time.deltaTime;
            }
        }


    }


}
