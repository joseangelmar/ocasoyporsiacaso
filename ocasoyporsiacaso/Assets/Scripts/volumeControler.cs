using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class volumeControler : MonoBehaviour
{
    [SerializeField] private AudioMixer myAudioMixer;

    public void SetVolume(float slider)
    {
        myAudioMixer.SetFloat("MasterVolume", Mathf.Log10(slider) * 20);
    }
}
